#!/usr/bin/env python
import os
import subprocess


def main():
    dir = "/var/www/flarum"

    # Check if dir exists and is empty
    if os.path.isdir(dir) and not os.listdir(dir):
        # change into the directory
        os.chdir(dir)
        # Install flarum
        install_flarum()


def install_flarum():
    # run ls with subprocess
    subprocess.run(["composer", "create-project", "flarum/flarum", "."])

    # Install extensions
    extensions = [
        "flarum/extension-manager:*",
        "if/flajax",
        "fof/upload",
        "fof/nightmode",
        "webbinaro/flarum-calendar",
        "fof/sitemap",
    ]

    for e in extensions:
        subprocess.run(["composer", "require", e])


if __name__ == "__main__":
    main()
