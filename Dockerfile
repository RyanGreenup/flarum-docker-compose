FROM alpine


# Install php and caddy
# PHP could also be unpacked in /usr/local/bin/ from <https://www.php.net/downloads.php>
# Exif extension is needed for file uploads
RUN apk add --no-interactive caddy php php-fpm composer \
     php-pdo php-pdo_mysql php-dom php-session          \
     php-gd php-tokenizer                               \
     php-fileinfo  php-exif

# Note that
# Installing an extension in Alpine, automatically adds it to the config with a hook like:
    # echo extension=pdo > /etc/php82/conf.d/00_pdo.ini

# Hardlink the latest version of php-fpm because alpine numbers the binaries
RUN ln "$(ls /usr/sbin/php-fpm* | head -n 1)" /usr/sbin/php-fpm

# Modify the config to allow large uploads
# Do it for every version of php by checking each php dir for a ./conf.d/ and adding it in there.
RUN for d in $(ls /etc/php*); do if [ -d conf.d ]; then printf 'upload_max_filesize = 8000M\n post_max_size = 8000M\n' > /etc/${d}/conf.d/zz_php-custom.ini ; fi; done

# Install config files
COPY ./php-fpm.conf /etc/php-fpm.conf
COPY ./Caddyfile /etc/Caddyfile

# Install the initializer (written in python not POSIX because life is too short)
# This will check if the directory has flarum and install it if not
COPY ./init.sh /usr/local/bin/initialize-flarum

# Run the container
CMD sh /usr/local/bin/initialize-flarum && \
    php-fpm -R -y /etc/php-fpm.conf     && \
    caddy run --config /etc/Caddyfile
