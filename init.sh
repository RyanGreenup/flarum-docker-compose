#!/bin/sh
dir="/var/www/flarum"

install_flarum() {
    # Download Flarum
    composer create-project flarum/flarum .

    # Install extensions
    extensions="flarum/extension-manager:* if/flajax fof/upload fof/nightmode webbinaro/flarum-calendar fof/sitemap"

    for e in $extensions
    do
        composer require $e
    done
}

main() {
    # Check if dir exists and is empty
    if [ -d "$dir" ] && [ -z "$(ls -A "$dir")" ]; then
        # change into the directory
        cd "$dir"
        # Install flarum
        install_flarum
    fi
}

main
